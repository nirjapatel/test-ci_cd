function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["calendar-calendar-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCalendarCalendarPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n  \n      <ion-title size=\"small\">{{viewTitle}}</ion-title>\n      <ion-buttons slot=\"end\">\n        <button ion-button [disabled]=\"isToday\" (click)=\"today()\">Today</button>\n        <button ion-button (click)=\"changeMode('month')\">M</button>\n        <button ion-button (click)=\"changeMode('week')\">W</button>\n        <button ion-button (click)=\"changeMode('day')\">D</button>\n        <button ion-button (click)=\"loadEvents()\">Load Events</button>\n    </ion-buttons>\n</ion-toolbar>\n     \n</ion-header>\n\n<ion-content>\n  <calendar [eventSource]=\"eventSource\"\n  [calendarMode]=\"calendar.mode\"\n  [currentDate]=\"calendar.currentDate\"\n  (onCurrentDateChanged)=\"onCurrentDateChanged($event)\"\n  (onRangeChanged)=\"reloadSource(startTime, endTime)\"\n  (onEventSelected)=\"onEventSelected($event)\"\n  (onTitleChanged)=\"onViewTitleChanged($event)\"\n  (onTimeSelected)=\"onTimeSelected($event)\"\n  step=\"30\">\n  \n</calendar>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/calendar/calendar-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/calendar/calendar-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: CalendarPageRoutingModule */

  /***/
  function srcAppCalendarCalendarRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarPageRoutingModule", function () {
      return CalendarPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _calendar_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./calendar.page */
    "./src/app/calendar/calendar.page.ts");

    var routes = [{
      path: '',
      component: _calendar_page__WEBPACK_IMPORTED_MODULE_3__["CalendarPage"]
    }];

    var CalendarPageRoutingModule = function CalendarPageRoutingModule() {
      _classCallCheck(this, CalendarPageRoutingModule);
    };

    CalendarPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CalendarPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/calendar/calendar.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/calendar/calendar.module.ts ***!
    \*********************************************/

  /*! exports provided: CalendarPageModule */

  /***/
  function srcAppCalendarCalendarModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarPageModule", function () {
      return CalendarPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _calendar_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./calendar-routing.module */
    "./src/app/calendar/calendar-routing.module.ts");
    /* harmony import */


    var _calendar_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./calendar.page */
    "./src/app/calendar/calendar.page.ts");
    /* harmony import */


    var ionic2_calendar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ionic2-calendar */
    "./node_modules/ionic2-calendar/__ivy_ngcc__/fesm2015/ionic2-calendar.js");

    var CalendarPageModule = function CalendarPageModule() {
      _classCallCheck(this, CalendarPageModule);
    };

    CalendarPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _calendar_routing_module__WEBPACK_IMPORTED_MODULE_5__["CalendarPageRoutingModule"], ionic2_calendar__WEBPACK_IMPORTED_MODULE_7__["NgCalendarModule"]],
      declarations: [_calendar_page__WEBPACK_IMPORTED_MODULE_6__["CalendarPage"]]
    })], CalendarPageModule);
    /***/
  },

  /***/
  "./src/app/calendar/calendar.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/calendar/calendar.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppCalendarCalendarPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhbGVuZGFyL2NhbGVuZGFyLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/calendar/calendar.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/calendar/calendar.page.ts ***!
    \*******************************************/

  /*! exports provided: CalendarPage */

  /***/
  function srcAppCalendarCalendarPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CalendarPage", function () {
      return CalendarPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CalendarPage = /*#__PURE__*/function () {
      function CalendarPage() {
        _classCallCheck(this, CalendarPage);

        this.calendar = {
          mode: 'month',
          currentDate: new Date(),
          dateFormatter: {
            formatMonthViewDay: function formatMonthViewDay(date) {
              return date.getDate().toString();
            },
            formatMonthViewDayHeader: function formatMonthViewDayHeader(date) {
              return 'MonMH';
            },
            formatMonthViewTitle: function formatMonthViewTitle(date) {
              return 'testMT';
            },
            formatWeekViewDayHeader: function formatWeekViewDayHeader(date) {
              return 'MonWH';
            },
            formatWeekViewTitle: function formatWeekViewTitle(date) {
              return 'testWT';
            },
            formatWeekViewHourColumn: function formatWeekViewHourColumn(date) {
              return 'testWH';
            },
            formatDayViewHourColumn: function formatDayViewHourColumn(date) {
              return 'testDH';
            },
            formatDayViewTitle: function formatDayViewTitle(date) {
              return 'testDT';
            }
          }
        };

        this.markDisabled = function (date) {
          var current = new Date();
          current.setHours(0, 0, 0);
          return date < current;
        };
      }

      _createClass(CalendarPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "loadEvents",
        value: function loadEvents() {
          this.eventSource = this.createRandomEvents();
        }
      }, {
        key: "onViewTitleChanged",
        value: function onViewTitleChanged(title) {
          this.viewTitle = title;
        }
      }, {
        key: "onEventSelected",
        value: function onEventSelected(event) {
          console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
        }
      }, {
        key: "changeMode",
        value: function changeMode(mode) {
          this.calendar.mode = mode;
        }
      }, {
        key: "today",
        value: function today() {
          this.calendar.currentDate = new Date();
        }
      }, {
        key: "onTimeSelected",
        value: function onTimeSelected(ev) {
          console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' + (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
        }
      }, {
        key: "onCurrentDateChanged",
        value: function onCurrentDateChanged(event) {
          var today = new Date();
          today.setHours(0, 0, 0, 0);
          event.setHours(0, 0, 0, 0);
          this.isToday = today.getTime() === event.getTime();
        }
      }, {
        key: "createRandomEvents",
        value: function createRandomEvents() {
          var events = [];

          for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;

            if (eventType === 0) {
              startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));

              if (endDay === startDay) {
                endDay += 1;
              }

              endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
              events.push({
                title: 'All Day - ' + i,
                startTime: startTime,
                endTime: endTime,
                allDay: true
              });
            } else {
              var startMinute = Math.floor(Math.random() * 24 * 60);
              var endMinute = Math.floor(Math.random() * 180) + startMinute;
              startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
              endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
              events.push({
                title: 'Event - ' + i,
                startTime: startTime,
                endTime: endTime,
                allDay: false
              });
            }
          }

          return events;
        }
      }, {
        key: "onRangeChanged",
        value: function onRangeChanged(ev) {
          console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
        }
      }]);

      return CalendarPage;
    }();

    CalendarPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-calendar',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./calendar.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar/calendar.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./calendar.page.scss */
      "./src/app/calendar/calendar.page.scss"))["default"]]
    })], CalendarPage);
    /***/
  }
}]);
//# sourceMappingURL=calendar-calendar-module-es5.js.map