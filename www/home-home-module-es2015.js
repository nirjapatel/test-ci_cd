(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title size=\"small\">\n      My Calendar\n    </ion-title>\n    <ion-button slot=\"end\" [routerLink]=\"['/calendar']\" routerLinkActive=\"router-link-active\" >Calendar View</ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-list>\n    <ion-item *ngFor=\"let cal of calendars\" tappable>\n      <button ion-button slot=\"start\" fill=\"clear\" (click)=\"addEvent(cal)\">\n        <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\n      </button>\n      {{cal.name}}\n      <p>{{cal.type}}</p>\n      <button ion-button  slot=\"end\" fill=\"clear\"  (click)=\"openCal(cal)\">\n        <ion-icon slot=\"icon-only\" name=\"arrow-forward\"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("page-home [col-1] {\n  flex: 0 0 14.2857142857%;\n  width: 14.2857142857%;\n  max-width: 14.2857142857%;\n}\npage-home .col {\n  text-align: center;\n  padding: 5px;\n}\npage-home .last-month, page-home .next-month {\n  color: #999999;\n  font-size: 90%;\n}\npage-home .currentDate, page-home .otherDate {\n  padding: 5px;\n}\npage-home .currentDate {\n  font-weight: bold;\n  background-color: red;\n  color: white;\n  border-radius: 30px;\n}\npage-home .calendar-header {\n  background-color: #1a732d;\n  color: #FFFFFF;\n}\npage-home .event-bullet {\n  margin: 2px auto;\n  height: 5px;\n  width: 5px;\n  background-color: green;\n  border-radius: 30px;\n}\npage-home .selected-date {\n  width: 20px;\n  height: 2px;\n  background-color: blue;\n}\npage-home .unselected-date {\n  border: none;\n}\npage-home .calendar-body .grid {\n  padding: 0;\n}\npage-home .calendar-body .col:last-child {\n  border-right: none;\n}\npage-home .calendar-body .calendar-weekday, page-home .calendar-body .calendar-date {\n  text-align: center;\n  margin: 0;\n}\npage-home .calendar-body .calendar-weekday {\n  font-weight: bold;\n  border-bottom: solid 1px #1a732d;\n  background-color: #26ab56;\n}\npage-home .calendar-body .calendar-date {\n  border-top: solid 1px #1a732d;\n  border-bottom: solid 1px #1a732d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaXJqYXBhdGVsL0Rlc2t0b3AvQ2FsZW5kYXJFdmVudHMvdGVzdC1jaV9jZC9zcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFJRSx3QkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNBSjtBREVFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0FDQUo7QURFRTtFQUNFLGNBQUE7RUFDQSxjQUFBO0FDQUo7QURFRTtFQUNFLFlBQUE7QUNBSjtBREVFO0VBQ0UsaUJBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ0FKO0FERUU7RUFDRSx5QkFBQTtFQUNBLGNBQUE7QUNBSjtBREVFO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNBSjtBREVFO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBQ0FKO0FERUU7RUFDRSxZQUFBO0FDQUo7QURHSTtFQUNFLFVBQUE7QUNETjtBREdJO0VBQ0Usa0JBQUE7QUNETjtBREdJO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0FDRE47QURHSTtFQUNFLGlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSx5QkFBQTtBQ0ROO0FER0k7RUFDRSw2QkFBQTtFQUNBLGdDQUFBO0FDRE4iLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsicGFnZS1ob21lIHtcbiAgW2NvbC0xXSB7XG4gICAgLXdlYmtpdC1ib3gtZmxleDogMDtcbiAgICAtd2Via2l0LWZsZXg6IDAgMCA4LjMzMzMzJTtcbiAgICAtbXMtZmxleDogMCAwIDguMzMzMzMlO1xuICAgIGZsZXg6IDAgMCAxNC4yODU3MTQyODU3MTQyODYlO1xuICAgIHdpZHRoOiAxNC4yODU3MTQyODU3MTQyODYlO1xuICAgIG1heC13aWR0aDogMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbiAgfVxuICAuY29sIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogNXB4O1xuICB9XG4gIC5sYXN0LW1vbnRoLCAubmV4dC1tb250aCB7XG4gICAgY29sb3I6ICM5OTk5OTk7XG4gICAgZm9udC1zaXplOiA5MCU7XG4gIH1cbiAgLmN1cnJlbnREYXRlLCAub3RoZXJEYXRlIHtcbiAgICBwYWRkaW5nOiA1cHg7XG4gIH1cbiAgLmN1cnJlbnREYXRlIHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIH1cbiAgLmNhbGVuZGFyLWhlYWRlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzFhNzMyZDtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbiAgfVxuICAuZXZlbnQtYnVsbGV0IHtcbiAgICBtYXJnaW46IDJweCBhdXRvO1xuICAgIGhlaWdodDogNXB4O1xuICAgIHdpZHRoOiA1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgfVxuICAuc2VsZWN0ZWQtZGF0ZSB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAycHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbiAgfVxuICAudW5zZWxlY3RlZC1kYXRlIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gIH1cbiAgLmNhbGVuZGFyLWJvZHkge1xuICAgIC5ncmlkIHtcbiAgICAgIHBhZGRpbmc6IDA7XG4gICAgfVxuICAgIC5jb2w6bGFzdC1jaGlsZCB7XG4gICAgICBib3JkZXItcmlnaHQ6IG5vbmU7XG4gICAgfVxuICAgIC5jYWxlbmRhci13ZWVrZGF5LCAuY2FsZW5kYXItZGF0ZSB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW46IDA7XG4gICAgfVxuICAgIC5jYWxlbmRhci13ZWVrZGF5IHtcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICMxYTczMmQ7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhYjU2O1xuICAgIH1cbiAgICAuY2FsZW5kYXItZGF0ZSB7XG4gICAgICBib3JkZXItdG9wOiBzb2xpZCAxcHggIzFhNzMyZDtcbiAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMWE3MzJkO1xuICAgIH1cbiAgfVxufSIsInBhZ2UtaG9tZSBbY29sLTFdIHtcbiAgLXdlYmtpdC1ib3gtZmxleDogMDtcbiAgLXdlYmtpdC1mbGV4OiAwIDAgOC4zMzMzMyU7XG4gIC1tcy1mbGV4OiAwIDAgOC4zMzMzMyU7XG4gIGZsZXg6IDAgMCAxNC4yODU3MTQyODU3JTtcbiAgd2lkdGg6IDE0LjI4NTcxNDI4NTclO1xuICBtYXgtd2lkdGg6IDE0LjI4NTcxNDI4NTclO1xufVxucGFnZS1ob21lIC5jb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDVweDtcbn1cbnBhZ2UtaG9tZSAubGFzdC1tb250aCwgcGFnZS1ob21lIC5uZXh0LW1vbnRoIHtcbiAgY29sb3I6ICM5OTk5OTk7XG4gIGZvbnQtc2l6ZTogOTAlO1xufVxucGFnZS1ob21lIC5jdXJyZW50RGF0ZSwgcGFnZS1ob21lIC5vdGhlckRhdGUge1xuICBwYWRkaW5nOiA1cHg7XG59XG5wYWdlLWhvbWUgLmN1cnJlbnREYXRlIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxucGFnZS1ob21lIC5jYWxlbmRhci1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWE3MzJkO1xuICBjb2xvcjogI0ZGRkZGRjtcbn1cbnBhZ2UtaG9tZSAuZXZlbnQtYnVsbGV0IHtcbiAgbWFyZ2luOiAycHggYXV0bztcbiAgaGVpZ2h0OiA1cHg7XG4gIHdpZHRoOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxucGFnZS1ob21lIC5zZWxlY3RlZC1kYXRlIHtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMnB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xufVxucGFnZS1ob21lIC51bnNlbGVjdGVkLWRhdGUge1xuICBib3JkZXI6IG5vbmU7XG59XG5wYWdlLWhvbWUgLmNhbGVuZGFyLWJvZHkgLmdyaWQge1xuICBwYWRkaW5nOiAwO1xufVxucGFnZS1ob21lIC5jYWxlbmRhci1ib2R5IC5jb2w6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1yaWdodDogbm9uZTtcbn1cbnBhZ2UtaG9tZSAuY2FsZW5kYXItYm9keSAuY2FsZW5kYXItd2Vla2RheSwgcGFnZS1ob21lIC5jYWxlbmRhci1ib2R5IC5jYWxlbmRhci1kYXRlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG59XG5wYWdlLWhvbWUgLmNhbGVuZGFyLWJvZHkgLmNhbGVuZGFyLXdlZWtkYXkge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICMxYTczMmQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyNmFiNTY7XG59XG5wYWdlLWhvbWUgLmNhbGVuZGFyLWJvZHkgLmNhbGVuZGFyLWRhdGUge1xuICBib3JkZXItdG9wOiBzb2xpZCAxcHggIzFhNzMyZDtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICMxYTczMmQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/calendar/ngx */ "./node_modules/@ionic-native/calendar/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let HomePage = class HomePage {
    constructor(calendar, platform, router) {
        this.calendar = calendar;
        this.platform = platform;
        this.router = router;
        this.calendars = [];
        this.platform.ready().then(() => {
            this.calendar.listCalendars().then(data => {
                this.calendars = data;
                console.log("Calendar data is ", this.calendars);
            });
        });
    }
    addEvent(cal) {
        let date = new Date();
        let options = { calendarId: cal.id, calendarName: cal.name, url: "", firstReminderMinutes: 15 };
        this.calendar.createEventInteractivelyWithOptions('My new Event', 'Family meet', 'Some Special Notes', date, date, options).then(() => {
        });
    }
    openCal(cal) {
        let navigationExtras = {
            state: {
                cal: cal
            }
        };
        this.router.navigate(['/calendar-detail'], navigationExtras);
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__["Calendar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map