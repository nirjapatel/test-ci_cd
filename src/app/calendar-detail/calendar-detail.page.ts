import { Component, OnInit } from '@angular/core';
import { Calendar } from '@ionic-native/calendar/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-calendar-detail',
  templateUrl: './calendar-detail.page.html',
  styleUrls: ['./calendar-detail.page.scss'],
})
export class CalendarDetailPage implements OnInit {
public calName='';
public events =[];
  constructor(private calendar:Calendar,private activatedRoute:ActivatedRoute,public router:Router,private platform:Platform) {
    this.activatedRoute.queryParams.subscribe(() => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.calName = this.router.getCurrentNavigation().extras.state.cal.name;
        console.log("CalName is ",this.calName);
        
      }
    });
    if(this.platform.is('ios')){
      this.calendar.findAllEventsInNamedCalendar(this.calName).then((data)=>{
        this.events = data;
        console.log("Calendar events in ios are :",this.events);
        
      },(error)=>{
        console.log("Error is geting ios events",error);
        
      })
    }
    else if(this.platform.is('android')){
      let start =new Date();
      let end =new Date();
      end.setDate(end.getDate() + 31);
      this.calendar.listEventsInRange(start,end).then((data)=>{
        this.events = data;
        console.log("Calendar events in android are :",this.events);
      })
    }
   }

  ngOnInit() {
    
  }

}
