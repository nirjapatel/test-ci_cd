import { Component } from '@angular/core';
import { Calendar } from '@ionic-native/calendar/ngx';
import { Platform } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
public calendars =[];
  constructor(private calendar:Calendar,public platform:Platform,public router:Router) {
    this.platform.ready().then(()=>{
      this.calendar.listCalendars().then(data =>{
        this.calendars = data;
        console.log("Calendar data is ",this.calendars);
        
      })
    });
  }
  addEvent(cal){
    let date= new Date();
    let options ={calendarId:cal.id,calendarName:cal.name,url:"",firstReminderMinutes:15};
    this.calendar.createEventInteractivelyWithOptions('My new Event','Family meet','Some Special Notes',date,date,options).then(()=>{

    })
  }
  openCal(cal){
    let navigationExtras: NavigationExtras = {
      state: {
        cal:cal
      }
    };
    this.router.navigate(['/calendar-detail'], navigationExtras);
  }
}
