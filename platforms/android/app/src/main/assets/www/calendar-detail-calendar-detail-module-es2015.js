(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["calendar-detail-calendar-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-detail/calendar-detail.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-detail/calendar-detail.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>{{ calName }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card *ngFor=\"let ev of events\">\n  <ion-card-header>\n    {{ev.title}}\n  </ion-card-header>\n  <ion-card-content>\n    <ion-list no-lines>\n      <ion-item>\n        <ion-icon name=\"calendar\"></ion-icon>Start :{{ev.dtstart | date:short}}\n      </ion-item>\n      <ion-item>\n        <ion-icon name=\"calendar\"></ion-icon>End :{{ev.dtend | date:short}}\n      </ion-item>\n      <ion-item>\n        <ion-icon name=\"locate\"></ion-icon>{{ev.eventLocation}}\n      </ion-item>\n    </ion-list>\n  </ion-card-content>\n</ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/calendar-detail/calendar-detail-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/calendar-detail/calendar-detail-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: CalendarDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarDetailPageRoutingModule", function() { return CalendarDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _calendar_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./calendar-detail.page */ "./src/app/calendar-detail/calendar-detail.page.ts");




const routes = [
    {
        path: '',
        component: _calendar_detail_page__WEBPACK_IMPORTED_MODULE_3__["CalendarDetailPage"]
    }
];
let CalendarDetailPageRoutingModule = class CalendarDetailPageRoutingModule {
};
CalendarDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CalendarDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/calendar-detail/calendar-detail.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/calendar-detail/calendar-detail.module.ts ***!
  \***********************************************************/
/*! exports provided: CalendarDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarDetailPageModule", function() { return CalendarDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _calendar_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./calendar-detail-routing.module */ "./src/app/calendar-detail/calendar-detail-routing.module.ts");
/* harmony import */ var _calendar_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./calendar-detail.page */ "./src/app/calendar-detail/calendar-detail.page.ts");







let CalendarDetailPageModule = class CalendarDetailPageModule {
};
CalendarDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _calendar_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["CalendarDetailPageRoutingModule"]
        ],
        declarations: [_calendar_detail_page__WEBPACK_IMPORTED_MODULE_6__["CalendarDetailPage"]]
    })
], CalendarDetailPageModule);



/***/ }),

/***/ "./src/app/calendar-detail/calendar-detail.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/calendar-detail/calendar-detail.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhbGVuZGFyLWRldGFpbC9jYWxlbmRhci1kZXRhaWwucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/calendar-detail/calendar-detail.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/calendar-detail/calendar-detail.page.ts ***!
  \*********************************************************/
/*! exports provided: CalendarDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CalendarDetailPage", function() { return CalendarDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/calendar/ngx */ "./node_modules/@ionic-native/calendar/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let CalendarDetailPage = class CalendarDetailPage {
    constructor(calendar, activatedRoute, router, platform) {
        this.calendar = calendar;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.platform = platform;
        this.calName = '';
        this.events = [];
        this.activatedRoute.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.calName = this.router.getCurrentNavigation().extras.state.cal.name;
                console.log("CalName is ", this.calName);
            }
        });
        if (this.platform.is('ios')) {
            this.calendar.findAllEventsInNamedCalendar(this.calName).then((data) => {
                this.events = data;
                console.log("Calendar events in ios are :", this.events);
            }, (error) => {
                console.log("Error is geting ios events", error);
            });
        }
        else if (this.platform.is('android')) {
            let start = new Date();
            let end = new Date();
            end.setDate(end.getDate() + 31);
            this.calendar.listEventsInRange(start, end).then((data) => {
                this.events = data;
                console.log("Calendar events in android are :", this.events);
            });
        }
    }
    ngOnInit() {
    }
};
CalendarDetailPage.ctorParameters = () => [
    { type: _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__["Calendar"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] }
];
CalendarDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-calendar-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./calendar-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/calendar-detail/calendar-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./calendar-detail.page.scss */ "./src/app/calendar-detail/calendar-detail.page.scss")).default]
    })
], CalendarDetailPage);



/***/ })

}]);
//# sourceMappingURL=calendar-detail-calendar-detail-module-es2015.js.map