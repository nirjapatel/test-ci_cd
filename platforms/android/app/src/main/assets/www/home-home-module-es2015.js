(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title size=\"small\">\n      My Calendar\n    </ion-title>\n    <ion-button slot=\"end\" [routerLink]=\"['/calendar']\" routerLinkActive=\"router-link-active\" >Calendar View</ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\">\n  <ion-list>\n    <ion-item *ngFor=\"let cal of calendars\" tappable>\n      <button ion-button slot=\"start\" fill=\"clear\" (click)=\"addEvent(cal)\">\n        <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\n      </button>\n      {{cal.name}}\n      <p>{{cal.type}}</p>\n      <button ion-button  slot=\"end\" fill=\"clear\"  (click)=\"openCal(cal)\">\n        <ion-icon slot=\"icon-only\" name=\"arrow-forward\"></ion-icon>\n      </button>\n    </ion-item>\n  </ion-list>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("page-home [col-1] {\n  flex: 0 0 14.2857142857%;\n  width: 14.2857142857%;\n  max-width: 14.2857142857%;\n}\npage-home .col {\n  text-align: center;\n  padding: 5px;\n}\npage-home .last-month, page-home .next-month {\n  color: #999999;\n  font-size: 90%;\n}\npage-home .currentDate, page-home .otherDate {\n  padding: 5px;\n}\npage-home .currentDate {\n  font-weight: bold;\n  background-color: red;\n  color: white;\n  border-radius: 30px;\n}\npage-home .calendar-header {\n  background-color: #1a732d;\n  color: #FFFFFF;\n}\npage-home .event-bullet {\n  margin: 2px auto;\n  height: 5px;\n  width: 5px;\n  background-color: green;\n  border-radius: 30px;\n}\npage-home .selected-date {\n  width: 20px;\n  height: 2px;\n  background-color: blue;\n}\npage-home .unselected-date {\n  border: none;\n}\npage-home .calendar-body .grid {\n  padding: 0;\n}\npage-home .calendar-body .col:last-child {\n  border-right: none;\n}\npage-home .calendar-body .calendar-weekday, page-home .calendar-body .calendar-date {\n  text-align: center;\n  margin: 0;\n}\npage-home .calendar-body .calendar-weekday {\n  font-weight: bold;\n  border-bottom: solid 1px #1a732d;\n  background-color: #26ab56;\n}\npage-home .calendar-body .calendar-date {\n  border-top: solid 1px #1a732d;\n  border-bottom: solid 1px #1a732d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaXJqYXBhdGVsL0Rlc2t0b3AvQ2FsZW5kYXJFdmVudHMvc3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBSUUsd0JBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0FDQUo7QURFRTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQ0FKO0FERUU7RUFDRSxZQUFBO0FDQUo7QURFRTtFQUNFLGlCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNBSjtBREVFO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0FDQUo7QURFRTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FDQUo7QURFRTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUNBSjtBREVFO0VBQ0UsWUFBQTtBQ0FKO0FER0k7RUFDRSxVQUFBO0FDRE47QURHSTtFQUNFLGtCQUFBO0FDRE47QURHSTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtBQ0ROO0FER0k7RUFDRSxpQkFBQTtFQUNBLGdDQUFBO0VBQ0EseUJBQUE7QUNETjtBREdJO0VBQ0UsNkJBQUE7RUFDQSxnQ0FBQTtBQ0ROIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInBhZ2UtaG9tZSB7XG4gIFtjb2wtMV0ge1xuICAgIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gICAgLXdlYmtpdC1mbGV4OiAwIDAgOC4zMzMzMyU7XG4gICAgLW1zLWZsZXg6IDAgMCA4LjMzMzMzJTtcbiAgICBmbGV4OiAwIDAgMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbiAgICB3aWR0aDogMTQuMjg1NzE0Mjg1NzE0Mjg2JTtcbiAgICBtYXgtd2lkdGg6IDE0LjI4NTcxNDI4NTcxNDI4NiU7XG4gIH1cbiAgLmNvbCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgfVxuICAubGFzdC1tb250aCwgLm5leHQtbW9udGgge1xuICAgIGNvbG9yOiAjOTk5OTk5O1xuICAgIGZvbnQtc2l6ZTogOTAlO1xuICB9XG4gIC5jdXJyZW50RGF0ZSwgLm90aGVyRGF0ZSB7XG4gICAgcGFkZGluZzogNXB4O1xuICB9XG4gIC5jdXJyZW50RGF0ZSB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICB9XG4gIC5jYWxlbmRhci1oZWFkZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxYTczMmQ7XG4gICAgY29sb3I6ICNGRkZGRkY7XG4gIH1cbiAgLmV2ZW50LWJ1bGxldCB7XG4gICAgbWFyZ2luOiAycHggYXV0bztcbiAgICBoZWlnaHQ6IDVweDtcbiAgICB3aWR0aDogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIH1cbiAgLnNlbGVjdGVkLWRhdGUge1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIGhlaWdodDogMnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XG4gIH1cbiAgLnVuc2VsZWN0ZWQtZGF0ZSB7XG4gICAgYm9yZGVyOiBub25lO1xuICB9XG4gIC5jYWxlbmRhci1ib2R5IHtcbiAgICAuZ3JpZCB7XG4gICAgICBwYWRkaW5nOiAwO1xuICAgIH1cbiAgICAuY29sOmxhc3QtY2hpbGQge1xuICAgICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgIH1cbiAgICAuY2FsZW5kYXItd2Vla2RheSwgLmNhbGVuZGFyLWRhdGUge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgbWFyZ2luOiAwO1xuICAgIH1cbiAgICAuY2FsZW5kYXItd2Vla2RheSB7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMWE3MzJkO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzI2YWI1NjtcbiAgICB9XG4gICAgLmNhbGVuZGFyLWRhdGUge1xuICAgICAgYm9yZGVyLXRvcDogc29saWQgMXB4ICMxYTczMmQ7XG4gICAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggIzFhNzMyZDtcbiAgICB9XG4gIH1cbn0iLCJwYWdlLWhvbWUgW2NvbC0xXSB7XG4gIC13ZWJraXQtYm94LWZsZXg6IDA7XG4gIC13ZWJraXQtZmxleDogMCAwIDguMzMzMzMlO1xuICAtbXMtZmxleDogMCAwIDguMzMzMzMlO1xuICBmbGV4OiAwIDAgMTQuMjg1NzE0Mjg1NyU7XG4gIHdpZHRoOiAxNC4yODU3MTQyODU3JTtcbiAgbWF4LXdpZHRoOiAxNC4yODU3MTQyODU3JTtcbn1cbnBhZ2UtaG9tZSAuY29sIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHg7XG59XG5wYWdlLWhvbWUgLmxhc3QtbW9udGgsIHBhZ2UtaG9tZSAubmV4dC1tb250aCB7XG4gIGNvbG9yOiAjOTk5OTk5O1xuICBmb250LXNpemU6IDkwJTtcbn1cbnBhZ2UtaG9tZSAuY3VycmVudERhdGUsIHBhZ2UtaG9tZSAub3RoZXJEYXRlIHtcbiAgcGFkZGluZzogNXB4O1xufVxucGFnZS1ob21lIC5jdXJyZW50RGF0ZSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cbnBhZ2UtaG9tZSAuY2FsZW5kYXItaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFhNzMyZDtcbiAgY29sb3I6ICNGRkZGRkY7XG59XG5wYWdlLWhvbWUgLmV2ZW50LWJ1bGxldCB7XG4gIG1hcmdpbjogMnB4IGF1dG87XG4gIGhlaWdodDogNXB4O1xuICB3aWR0aDogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cbnBhZ2UtaG9tZSAuc2VsZWN0ZWQtZGF0ZSB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcbn1cbnBhZ2UtaG9tZSAudW5zZWxlY3RlZC1kYXRlIHtcbiAgYm9yZGVyOiBub25lO1xufVxucGFnZS1ob21lIC5jYWxlbmRhci1ib2R5IC5ncmlkIHtcbiAgcGFkZGluZzogMDtcbn1cbnBhZ2UtaG9tZSAuY2FsZW5kYXItYm9keSAuY29sOmxhc3QtY2hpbGQge1xuICBib3JkZXItcmlnaHQ6IG5vbmU7XG59XG5wYWdlLWhvbWUgLmNhbGVuZGFyLWJvZHkgLmNhbGVuZGFyLXdlZWtkYXksIHBhZ2UtaG9tZSAuY2FsZW5kYXItYm9keSAuY2FsZW5kYXItZGF0ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwO1xufVxucGFnZS1ob21lIC5jYWxlbmRhci1ib2R5IC5jYWxlbmRhci13ZWVrZGF5IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMWE3MzJkO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjZhYjU2O1xufVxucGFnZS1ob21lIC5jYWxlbmRhci1ib2R5IC5jYWxlbmRhci1kYXRlIHtcbiAgYm9yZGVyLXRvcDogc29saWQgMXB4ICMxYTczMmQ7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjMWE3MzJkO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/calendar/ngx */ "./node_modules/@ionic-native/calendar/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let HomePage = class HomePage {
    constructor(calendar, platform, router) {
        this.calendar = calendar;
        this.platform = platform;
        this.router = router;
        this.calendars = [];
        this.platform.ready().then(() => {
            this.calendar.listCalendars().then(data => {
                this.calendars = data;
                console.log("Calendar data is ", this.calendars);
            });
        });
    }
    addEvent(cal) {
        let date = new Date();
        let options = { calendarId: cal.id, calendarName: cal.name, url: "", firstReminderMinutes: 15 };
        this.calendar.createEventInteractivelyWithOptions('My new Event', 'Family meet', 'Some Special Notes', date, date, options).then(() => {
        });
    }
    openCal(cal) {
        let navigationExtras = {
            state: {
                cal: cal
            }
        };
        this.router.navigate(['/calendar-detail'], navigationExtras);
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_native_calendar_ngx__WEBPACK_IMPORTED_MODULE_2__["Calendar"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map